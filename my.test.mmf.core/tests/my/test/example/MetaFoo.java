package my.test.example;

import my.test.mmf.types.TAttr;
import my.test.mmf.types.TCompositeType;
import my.test.mmf.types.TRef;
import my.test.mmf.types.TypesHelper;

public interface MetaFoo extends TCompositeType{
	
	// TODO: add known types example here
	
	final TAttr<MetaFoo, TypesHelper.STRING> name = TypesHelper.attr();

	final static TRef<MetaFoo,MetaBar> unidirectionalBar = TypesHelper.ref();

	final TRef<MetaFoo,MetaBar> bidirectionalBar = TypesHelper.ref(MetaBar.bidirectionalFoo);
	
}
