package my.test.example;

import my.test.mmf.types.TCompositeType;
import my.test.mmf.types.TRef;
import my.test.mmf.types.TypesHelper;

public interface MetaBar extends TCompositeType {
	
	TRef<MetaBar,MetaFoo> bidirectionalFoo = TypesHelper.ref( MetaFoo.bidirectionalBar);
	
	

}
