package my.test.mmf.types;

public interface TRef<D extends TCompositeType, R extends TCompositeType> extends TStructuralFeature<D> {
	
	R getReferencedType();
	
	TRef<R,D> getOpposite();
}
