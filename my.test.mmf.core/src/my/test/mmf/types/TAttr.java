package my.test.mmf.types;

public interface TAttr<D extends TCompositeType, V extends TBaseType> extends TStructuralFeature<D>{

	V getValueType();
}
