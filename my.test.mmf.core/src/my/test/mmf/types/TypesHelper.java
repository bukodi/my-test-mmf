package my.test.mmf.types;

public class TypesHelper {
	
	public static <D extends TCompositeType, V extends TBaseType> TAttr<D,V> attr() {
		return new AttrImpl<D,V>();
	}

	private final static ThreadLocal<RefImpl<?, ?>> currentRef_TL = new ThreadLocal<>();  
	
	public static <D extends TCompositeType, R extends TCompositeType> TRef<D,R> ref() {
		try{
			RefImpl<D, R> ref = new RefImpl<D,R>();			
			currentRef_TL.set(ref);
			return ref;
		} finally {
			
		}
	}

	public static <D extends TCompositeType, R extends TCompositeType> TRef<D,R> ref( TRef<R,D> opposite ) {
		RefImpl<?, ?> currentRef = currentRef_TL.get();
		RefImpl<D, R> ref = new RefImpl<D,R>();	
		ref.opposite = opposite;
		return ref;
	}

	public static interface STRING extends TBaseType{};
	public static interface INTEGER extends TBaseType{};
	public static interface BOOLEAN extends TBaseType{};
	public static interface LONG extends TBaseType{};
	
	private static class FeatureImpl<D extends TCompositeType> implements TStructuralFeature<D>{
		D declaringType;
		String name;

		@Override
		public String getName() {
			return name;
		}

		@Override
		public D getDeclaringType() {
			return declaringType;
		}
		
		
	}

	private static class AttrImpl<D extends TCompositeType, V extends TBaseType> extends FeatureImpl<D> implements TAttr<D, V>{
		V valueType;

		@Override
		public V getValueType() {
			return valueType;
		}
	}

	private static class RefImpl<D extends TCompositeType, R extends TCompositeType> extends FeatureImpl<D> implements TRef<D, R> {
		R referencedType;
		TRef<R,D> opposite;

		@Override
		public R getReferencedType() {
			return referencedType;
		}
		@Override
		public TRef<R, D> getOpposite() {
			return opposite;
		}
		
	}
}
