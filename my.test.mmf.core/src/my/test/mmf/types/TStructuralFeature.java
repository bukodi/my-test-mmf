package my.test.mmf.types;

public interface TStructuralFeature<D extends TCompositeType> {

	String getName();
	
	D getDeclaringType();
}
